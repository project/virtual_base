# Virtual Base

Virtual Base is designed to address a specific use case: enabling an existing Drupal website to be accessed via additional URLs that include a specified path prefix. This functionality provides a virtual "RewriteBase" layered on top of the website.

For instance, consider a scenario where your website is typically accessed internally (without any RewriteBase) within a private organization. Simultaneously, it is also accessible through an external gateway that uses a fixed URL, with the Drupal website available under a specific path prefix.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/virtual_base).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/search/virtual_base).

## Table of contents

- Installation
- Configuration
- Maintainers

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

Configure at (/admin/config/system/virtual-base).

Since 1.0.0-alpha-4 version, you need to add instructions to your .htaccess file to make it work properly.

Inside the .htaccess file of Drupal, add the following lines, to cope with the path prefix `path_prefix`

```
# ---
# Virtual Base configuration
# ---

RewriteRule .* - [E=VIRTUAL_BASE:]

RewriteCond %{REQUEST_URI} ^/my_prefix
RewriteRule .* - [E=VIRTUAL_BASE:/my_prefix]

RewriteCond %{REQUEST_URI} ^/my_prefix
RewriteRule ^my_prefix/(.*)$ /$1 [L]

# cope with apache env variable rewritings when having urls redirects
RewriteCond %{ENV:REDIRECT_VIRTUAL_BASE} (.+)
RewriteRule .* - [E=VIRTUAL_BASE:%1]

```



If you want to enable the module for a specific domain, 
you can add a line `RewriteCond %{HTTP_HOST} ^specific\.domain_name\.com` 
and replace `specific\.domain_name\.com` with the domain name you want to restrict the module to.

```
# ---
# Virtual Base configuration
# ---

RewriteRule .* - [E=VIRTUAL_BASE:]

RewriteCond %{HTTP_HOST} ^specific\.domain_name\.com
RewriteCond %{REQUEST_URI} ^/my_prefix
RewriteRule .* - [E=VIRTUAL_BASE:/my_prefix]

RewriteCond %{HTTP_HOST} ^specific\.domain_name\.com
RewriteCond %{REQUEST_URI} ^/my_prefix
RewriteRule ^my_prefix/(.*)$ /$1 [L]

# cope with apache env variable rewritings when having urls redirects
RewriteCond %{ENV:REDIRECT_VIRTUAL_BASE} (.+)
RewriteRule .* - [E=VIRTUAL_BASE:%1]

```


## Maintainers

Current maintainers:

- [Mikael Meulle (just_like_good_vibes)](https://www.drupal.org/u/just_like_good_vibes)
- [Thomas Musa (musathomas)](https://www.drupal.org/u/musathomas)
