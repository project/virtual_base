<?php

namespace Drupal\Tests\virtual_base\Functional;

use Drupal\Tests\Traits\Core\PathAliasTestTrait;

/**
 * Tests help functionality for virtual base.
 *
 * @group virtual_base
 */
class VirtualBaseNodeAliasTest extends VirtualBaseTestBase {

  use PathAliasTestTrait;

  /**
   * Modules to enable.
   *
   * @var array<string>
   */
  protected static $modules = ['virtual_base', 'node', 'path'];

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    parent::setUp();
    // Create path alias of node.
    $path = '/node/' . $this->node->id();
    $alias = '/alias/test';
    $this->createPathAlias($path, $alias);
  }

  /**
   * Verifies that the node alias url is still working.
   */
  public function testNodeDefaultAlias() {
    $this->drupalGet('alias/test');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains($this->node->label());
  }

  /**
   * Verifies that the node alias url with virtual bas prefix works.
   */
  public function testNodeVirtualAlias() {
    $this->drupalGet($this->pathPrefixTest . '/alias/test');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains($this->node->label());
  }

}
