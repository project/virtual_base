<?php

namespace Drupal\Tests\virtual_base\Functional;

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\file\Entity\File;
use Drupal\image\Entity\ImageStyle;
use Drupal\media\Entity\Media;
use Drupal\Tests\media\Functional\MediaFunctionalTestTrait;
use Drupal\Tests\media\Traits\MediaTypeCreationTrait;
use Drupal\Tests\TestFileCreationTrait;

/**
 * Tests the image media source.
 *
 * @group media
 */
class VirtualBaseMediaTest extends VirtualBaseTestBase {

  use TestFileCreationTrait;

  use MediaFunctionalTestTrait;
  use MediaTypeCreationTrait;

  /**
   * Plugin ID.
   *
   * @var string
   */
  protected $pluginId;

  /**
   * Modules to enable.
   *
   * @var array<string>
   */
  protected static $modules = [
    'system',
    'node',
    'field_ui',
    'media',
    'media_test_source',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    // Create user.
    $admin_user = $this->drupalCreateUser([
      'administer content types',
      'administer nodes',
      'bypass node access',
      'access media overview',
      'administer media',
      'administer media fields',
      'administer media form display',
      'administer media display',
      'administer media types',
      'view media',
    ]);
    $this->drupalLogin($admin_user);
    $this->pluginId = 'image';
  }

  /**
   * Test media with virtual base.
   */
  public function testMedia() {
    // Create an image media type for testing.
    $this->createMediaType($this->pluginId, ['id' => 'image']);
    // Creates an entity reference field for media.
    $field_storage = FieldStorageConfig::create([
      'field_name' => 'field_media_reference',
      'type' => 'entity_reference',
      'entity_type' => 'node',
      'cardinality' => 1,
      'settings' => [
        'target_type' => 'media',
      ],
    ]);
    $field_storage->save();
    FieldConfig::create([
      'field_storage' => $field_storage,
      'bundle' => $this->testType,
      'label' => 'Reference media',
      'translatable' => FALSE,
    ])->save();

    // Alter the form display.
    $this->container->get('entity_display.repository')
      ->getFormDisplay('node', $this->testType)
      ->setComponent('field_media_reference', [
        'type' => 'entity_reference_autocomplete',
      ])
      ->save();

    // Create and upload a file to the media.
    $imageTestFiles = $this->getTestFiles('image');
    $currentImageTestFile = current($imageTestFiles);
    $file = File::create([
      'uri' => ($currentImageTestFile) ? ($currentImageTestFile)->uri : NULL,
    ]);
    $file->save();
    $mediaImage = Media::create([
      'bundle' => 'image',
      'name' => 'Test image',
      'field_media_image' => $file->id(),
    ]);
    $mediaImage->save();

    // Set the display on media with the image style.
    $this->setImageStyle($mediaImage);
    // Edit the  node.
    $edit = [
      'title[0][value]' => t('Virtual base test media'),
    ];
    $edit['field_media_reference[0][target_id]'] = $mediaImage->getName();
    $this->drupalGet('node/' . $this->node->id() . '/edit');
    $this->submitForm($edit, 'Save');
    // Set render media as default entity ref.
    $this->container->get('entity_display.repository')
      ->getViewDisplay('node', $this->testType, 'default')
      ->setComponent('field_media_reference', [
        'type' => 'entity_reference_entity_view',
        'settings' => [
          'view_mode' => 'default',
          'link' => FALSE,
        ],
        'region' => 'content',
      ])->save();
    $this->drupalGet('node/' . $this->node->id());
    $style = $this->getMediaImageStyle($mediaImage);

    // Validate the image being loaded with the media reference.
    $path = \Drupal::service('file_url_generator')
      ->generateString($style->buildUri($file->getFileUri()));
    // Test the tag image is here.
    $this->assertSession()
      ->responseContains('<img loading="lazy" src="' . $path);

    $this->drupalGet($this->pathPrefixTest . '/node/' . $this->node->id());
    // Test the tag image is here.
    $this->assertSession()
      ->responseContains('<img loading="lazy" src="' . $this->pathPrefixTest . $path);
  }

  /**
   * Set image style.
   *
   * @param \Drupal\media\Entity\Media $media_type
   *   Media type.
   *
   * @return int
   *   Result of save.
   */
  private function setImageStyle(Media $media_type): int {
    return $this->container->get('entity_display.repository')
      ->getViewDisplay('media', $media_type->bundle(), 'default')
      ->setComponent('field_media_image', [
        'type' => $this->pluginId,
        'settings' => [
          'image_link' => '',
          'image_style' => 'large',
          'image_loading' => ['attribute' => 'lazy'],
        ],
      ])->save();
  }

  /**
   * Get media image style.
   *
   * @param \Drupal\media\Entity\Media $media_type
   *   Media type.
   *
   * @return \Drupal\image\Entity\ImageStyle
   *   Image style loaded.
   */
  private function getMediaImageStyle(Media $media_type): ImageStyle {
    // Change the display to use the media.
    $component = $this->container->get('entity_display.repository')
      ->getViewDisplay('media', $media_type->bundle(), 'default')
      ->getComponent('field_media_image');
    // Test the component of image style is well set.
    $this->assertEquals($component['settings']['image_style'], 'large');
    return ImageStyle::load($component['settings']['image_style']);
  }

}
