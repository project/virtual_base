<?php

namespace Drupal\Tests\virtual_base\Functional;

/**
 * Tests help functionality for virtual base.
 *
 * @group virtual_base
 */
class VirtualBaseNodeTest extends VirtualBaseTestBase {

  /**
   * Verifies that the default node url is still working.
   */
  public function testNodeDefaultUrl() {
    // Check if the normal url is available.
    $this->drupalGet('node/' . $this->node->id());
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains($this->node->label());
  }

  /**
   * Verifies that the default node url with virtual prefix works.
   */
  public function testNodeVirtualUrl() {
    // Create node and check if the normal url is available.
    $this->drupalGet($this->pathPrefixTest . '/node/' . $this->node->id());
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains($this->node->label());
  }

}
