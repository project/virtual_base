<?php

namespace Drupal\Tests\virtual_base\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\Traits\Core\PathAliasTestTrait;

/**
 * Provides a base class for testing the Path module.
 */
class VirtualBaseFormTest extends BrowserTestBase {

  use PathAliasTestTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Test type.
   *
   * @var string
   */
  protected string $testType;

  /**
   * Admin Form Path.
   *
   * @var string
   */
  protected string $adminFormPath;

  /**
   * Path prefix.
   *
   * @var string
   */
  protected string $pathPrefixTest;

  /**
   * Modules to enable.
   *
   * @var array<string>
   */
  protected static $modules = ['virtual_base', 'node', 'path', 'path_alias'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {

    parent::setUp();
    // Create user.
    $admin_user = $this->drupalCreateUser([
      'administer content types',
      'administer nodes',
      'bypass node access',
      'administer site configuration',
      'administer url aliases',
      'create url aliases',
    ]);

    $this->drupalLogin($admin_user);

    $this->testType = 'virtual_base';
    $this->pathPrefixTest = '/virtual-test';
    $this->adminFormPath = Url::fromRoute('virtual_base.settings')->getInternalPath();
    // Create content type.
    $this->drupalCreateContentType([
      'type' => $this->testType,
    ]);
  }

  /**
   * Test Validate config form.
   */
  public function testValidateConfigForm() {
    // Create node and check if the normal url is available.
    $node = $this->drupalCreateNode([
      'type' => $this->testType,
    ]);

    $path = '/node/' . $node->id();
    $this->createPathAlias($path, $this->pathPrefixTest);
    $this->drupalGet($this->pathPrefixTest);
    $this->assertSession()->statusCodeEquals(200);

    $edit = [];
    $edit['enabled'] = TRUE;
    $edit['path_prefix'] = $this->pathPrefixTest;
    $this->drupalGet($this->adminFormPath);

    $this->assertSession()->statusCodeEquals(200);

    $this->submitForm($edit, 'Save configuration');
    $this->assertSession()->statusCodeEquals(200);

    $this->assertSession()
      ->pageTextContains(sprintf('The path prefix is already set as path alias of %s %d', 'node', $node->id()));
  }

  /**
   * Test validate node form.
   */
  public function testValidateNodeForm() {
    $this->config('virtual_base.settings')
      ->set('enabled', TRUE)
      ->set('path_prefix', $this->pathPrefixTest)
      ->save();
    // Create node and check if the normal url is available.
    $node = $this->drupalCreateNode([
      'type' => $this->testType,
      'title' => $this->randomMachineName(8),
    ]);

    $path = '/node/' . $node->id() . '/edit';
    $this->drupalGet($path);
    $this->assertSession()->statusCodeEquals(200);

    $edit = [];
    $edit['path[0][alias]'] = $this->pathPrefixTest;
    $this->submitForm($edit, 'Save');
    $this->assertSession()->statusCodeEquals(200);

    $this->assertSession()
      ->pageTextContains(sprintf('The path alias is already set as path prefix inside virtual base settings.'));
  }

}
