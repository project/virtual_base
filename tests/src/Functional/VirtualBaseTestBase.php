<?php

namespace Drupal\Tests\virtual_base\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Provides a base class for testing the Virtual base module.
 */
abstract class VirtualBaseTestBase extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array<string>
   */
  protected static $modules = ['virtual_base', 'node'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Test type.
   *
   * @var string
   */
  protected string $testType;

  /**
   * Path prefix.
   *
   * @var string
   */
  protected string $pathPrefixTest;

  /**
   * Node.
   *
   * @var \Drupal\node\NodeInterface|null
   */
  protected $node;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    // Create user.
    $admin_user = $this->drupalCreateUser([
      'administer content types',
      'administer nodes',
      'bypass node access',
    ]);

    $this->drupalLogin($admin_user);

    $this->testType = 'virtual_base';
    $this->pathPrefixTest = '/virtual-test';
    $this->config('virtual_base.settings')
      ->set('enabled', TRUE)
      ->set('path_prefix', $this->pathPrefixTest)
      ->save();
    // Create content type.
    $this->drupalCreateContentType([
      'type' => $this->testType,
    ]);

    // Create node.
    $node = $this->drupalCreateNode([
      'type' => $this->testType,
    ]);

    $this->node = $node;
  }

}
