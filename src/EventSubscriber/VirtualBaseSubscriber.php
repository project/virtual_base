<?php

namespace Drupal\virtual_base\EventSubscriber;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Render\HtmlResponse;
use Drupal\virtual_base\VirtualBaseManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Defines a test session subscriber that checks whether the session is empty.
 *
 *  @SuppressWarnings(PHPMD.CamelCaseParameterName)
 *  @SuppressWarnings(PHPMD.CamelCaseVariableName)
 *  @SuppressWarnings(PHPMD.LongClassName)
 *  @SuppressWarnings(PHPMD.LongVariable)
 */
class VirtualBaseSubscriber implements EventSubscriberInterface {

  /**
   * Constructs a new VirtualBaseSubscriber object.
   *
   * @param \Drupal\virtual_base\VirtualBaseManagerInterface $virtualBaseManager
   *   Service injected.
   */
  public function __construct(
    protected VirtualBaseManagerInterface $virtualBaseManager,
  ) {
  }

  /**
   * Callback kernel event on respond.
   *
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $event
   *   The Event to process.
   */
  public function onRespond(ResponseEvent $event) {
    $path_prefix = $this->virtualBaseManager->getPathPrefix();
    if (!$path_prefix) {
      return;
    }
    $response = $event->getResponse();
    if ($response instanceof HtmlResponse) {
      $altered_text = $this->virtualBaseManager->alterHtml($path_prefix, $response->getContent());
      if ($altered_text) {
        $cache_metadata = new CacheableMetadata();
        $cache_metadata->addCacheContexts(['virtual_base']);
        $response->addCacheableDependency($cache_metadata);
        $response->setContent($altered_text);
      }
      return;
    }
  }

  /**
   * Process the kernel request event.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   The event.
   */
  public function onRequest(RequestEvent $event) {
    $request = $event->getRequest();
    $uri = $request->getRequestUri();
    $this->virtualBaseManager->resolveInboundPath($uri);
  }

  /**
   * Registers the methods in this class that should be listeners.
   *
   * @return array
   *   An array of event listener definitions.
   */
  public static function getSubscribedEvents(): array {
    // BigPipe is handling the response event with weight -10000.
    $events[KernelEvents::RESPONSE][] = ['onRespond', -9999];
    $events[KernelEvents::REQUEST][] = ['onRequest', 20];
    return $events;
  }

}
