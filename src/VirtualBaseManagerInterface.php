<?php

namespace Drupal\virtual_base;

use Drupal\Core\Cache\CacheableMetadata;

/**
 * Interface for Service VirtualBaseManager.
 */
interface VirtualBaseManagerInterface {

  /**
   * Get the config (with caching).
   */
  public function getConfig();

  /**
   * Get the path prefix or null if not enabled or not set.
   *
   * The current request can be checked if it contains the path prefix.
   *
   * @param bool $checkEnv
   *   If true, the env is checked for the path prefix.
   *
   * @return string|null
   *   Returns the path prefix or null if not enabled or not set.
   */
  public function getPathPrefix(bool $checkEnv = TRUE) : ?string;

  /**
   * Resolves inbound path.
   *
   * @param string $path
   *   The path to process.
   *
   * @return string
   *   The processed path.
   */
  public function resolveInboundPath($path) : string;

  /**
   * Resolves Outbound path.
   *
   * @param string $path
   *   The path to process.
   * @param \Drupal\Core\Render\BubbleableMetadata $bubbleableMetadata
   *   (optional) Object to collect path processors' bubbleable metadata.
   *
   * @return string
   *   The processed path.
   */
  public function resolveOutboundPath($path, CacheableMetadata $bubbleableMetadata = NULL) : string;

  /**
   * Alter HTML content to add the path prefix.
   *
   * @param string $path_prefix
   *   The path prefix to use.
   * @param string $text
   *   The text to alter.
   *
   * @return string|null
   *   Returns the patched text or null if nothing was changed.
   */
  public function alterHtml(string $path_prefix, string $text) : ?string;

}
