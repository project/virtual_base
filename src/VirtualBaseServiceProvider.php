<?php

namespace Drupal\virtual_base;

use Drupal\Core\Config\BootstrapConfigStorageFactory;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Drupal\virtual_base\PathProcessor\VirtualBasePathProcessor;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Implement virtual base service provider.
 *
 *  @SuppressWarnings(PHPMD.CamelCaseParameterName)
 *  @SuppressWarnings(PHPMD.CamelCaseVariableName)
 *  @SuppressWarnings(PHPMD.LongClassName)
 *  @SuppressWarnings(PHPMD.LongVariable)
 */
class VirtualBaseServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container) {
    // Register virtual base service.
    $definition = $container->register(' virtual_base.path_processor', VirtualBasePathProcessor::class)
      ->addTag('path_processor_inbound', ['priority' => 301])
      ->addTag('path_processor_outbound', ['priority' => 301])
      ->addArgument(new Reference('config.factory'))
      ->addArgument(new Reference('virtual_base.manager'))
      ->addArgument(new Reference('module_handler'));
    // Add path alias manager if module exist.
    if ($container->hasDefinition('path_alias.manager')) {
      $definition->addArgument(new Reference('path_alias.manager'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    if ($container->hasDefinition('virtual_base.path_processor') && !$this->isEnabled()) {
      $container->getDefinition('virtual_base.path_processor')
        ->clearTag('path_processor_inbound')
        ->clearTag('path_processor_outbound');
    }
  }

  /**
   * Checks whether the site has virtual bas enabled.
   *
   * @return bool
   *   TRUE if the site has virtual base enabled, FALSE otherwise.
   */
  protected function isEnabled() {
    $config_storage = BootstrapConfigStorageFactory::get();
    $virtualBase = $config_storage->read('virtual_base.settings');
    return $virtualBase["enabled"];
  }

}
