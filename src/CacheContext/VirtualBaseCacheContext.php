<?php

namespace Drupal\virtual_base\CacheContext;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\Context\CacheContextInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\virtual_base\VirtualBaseManagerInterface;

/**
 * The virtual base Context.
 */
class VirtualBaseCacheContext implements CacheContextInterface {

  use StringTranslationTrait;

  /**
   * The constructor.
   *
   * @param \Drupal\virtual_base\VirtualBaseManagerInterface $virtualBaseManager
   *   The virtual base manager.
   */
  public function __construct(
    protected VirtualBaseManagerInterface $virtualBaseManager,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function getLabel() {
    return t('Virtual base cache context');
  }

  /**
   * {@inheritdoc}
   */
  public function getContext() {
    return $this->virtualBaseManager->getPathPrefix() ?? "";
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata() {
    return new CacheableMetadata();
  }

}
