<?php

namespace Drupal\virtual_base\Form;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteBuilderInterface;
use Drupal\Core\Url;
use Drupal\virtual_base\VirtualBaseManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Virtual base form settings.
 *
 * @SuppressWarnings(PHPMD.CamelCaseParameterName)
 * @SuppressWarnings(PHPMD.CamelCaseVariableName)
 * @SuppressWarnings(PHPMD.LongClassName)
 * @SuppressWarnings(PHPMD.LongVariable)
 */
class VirtualBaseSettingsForm extends ConfigFormBase {

  /**
   * Constructs a SecKitSettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typedConfigManager
   *   The typed config manager.
   * @param \Drupal\virtual_base\VirtualBaseManagerInterface $helper
   *   Helper.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   Module handler.
   * @param \Drupal\Core\Routing\RouteBuilderInterface $routeBuilder
   *   Container.
   */
  public function __construct(
    ConfigFactoryInterface $configFactory,
    TypedConfigManagerInterface $typedConfigManager,
    protected VirtualBaseManagerInterface $helper,
    protected ModuleHandlerInterface $moduleHandler,
    protected RouteBuilderInterface $routeBuilder,
  ) {
    parent::__construct($configFactory, $typedConfigManager);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('virtual_base.manager'),
      $container->get('module_handler'),
      $container->get('router.builder')

    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'virtual_base_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['virtual_base.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('virtual_base.settings');

    $form['enabled'] = [
      '#type' => 'checkbox',
      '#default_value' => $config->get('enabled'),
      '#title' => $this->t('Enabled'),
      '#return_value' => TRUE,
      '#description' => $this->t('Enable a virtual base.'),
    ];

    $form['path_prefix'] = [
      '#type' => 'textfield',
      '#maxlength' => 254,
      '#default_value' => $config->get('path_prefix'),
      '#title' => $this->t('Path Prefix'),
      '#description' => $this->t("Specify the path prefix to be the virtual base. it should starts with a /"),
      '#states' => [
        'required' => [
          'input[name="enabled"]' => ['checked' => TRUE],
        ],
        'visible' => [
          'input[name="enabled"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // If From-Origin is enabled, it should be explicitly set.
    $enabled = $form_state->getValue("enabled");
    $path_prefix = $form_state->getValue("path_prefix");
    $has_error = FALSE;
    if ($enabled) {
      if (!str_starts_with($path_prefix, "/")) {
        $form_state->setErrorByName("path_prefix", $this->t("The path prefix is invalid: it should start with a '/'"));
        $has_error = TRUE;
      }
      if (!$has_error && str_ends_with($path_prefix, "/")) {
        $form_state->setErrorByName("path_prefix", $this->t("The path prefix is invalid: it should not end with a '/'"));
      }
      if (!$has_error && UrlHelper::encodePath($path_prefix) !== $path_prefix) {
        $form_state->setErrorByName("path_prefix", $this->t("The path prefix is invalid: it should be properly encoded"));
      }
      // Avoid conflict with path alias if already set.
      // When the request hit this path we cannot know
      // if it's the home with path prefix or the path alias requested.
      if (!$has_error && $this->moduleHandler->moduleExists('path_alias')) {
        try {
          $url = NULL;
          try {
            $url = Url::fromUri('internal:' . $path_prefix);
            $url_options = $url->getOptions();
            $url_query = (is_array($url_options) && array_key_exists("query", $url_options)) ? $url_options["query"] : [];
            if (array_key_exists("_virtual_base", $url_query)) {
              // The path prefix provided starts with the current virtual base.
              $virtual_base_prefix = $url_query["_virtual_base"];
              if ($path_prefix == $virtual_base_prefix) {
                $url = NULL;
              }
              elseif (str_starts_with($path_prefix, $virtual_base_prefix)) {
                $path_suffix = substr($path_prefix, strlen($virtual_base_prefix));
                try {
                  $url = Url::fromUri('internal:' . $path_suffix);
                }
                catch (\InvalidArgumentException $e) {
                  $url = NULL;
                }
              }
            }
          }
          catch (\InvalidArgumentException $e) {
            $url = NULL;
          }
          if ($url && $url->isRouted()) {
            $params = $url->getRouteParameters();
            $entity_type = key($params);
            if (!empty($params)) {
              $form_state->setErrorByName("path_prefix", $this->t("The path prefix %prefix is invalid: it is already set as path alias of %entity_type %id. Route name is %route, url is %url_string", [
                '%prefix' => $path_prefix,
                '%entity_type' => $entity_type,
                '%id' => $params[$entity_type],
                '%route' => $url->getRouteName(),
                '%url_string' => $url->toString(),
              ]));
            }
          }
        }
        catch (\InvalidArgumentException $e) {
          $form_state->setErrorByName("", $this->t("The path prefix is invalid: %error", [
            '%error' => $e->getMessage(),
          ]));
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('virtual_base.settings');
    $changed = FALSE;
    $enabled_value = $form_state->getValue("enabled") ? TRUE : FALSE;
    if ($config->get("enabled") !== $enabled_value) {
      $config->set("enabled", $enabled_value);
      $changed = TRUE;
    }
    $path_prefix = $form_state->getValue("path_prefix") ?? "";
    if ($config->get("path_prefix") !== $path_prefix) {
      $config->set("path_prefix", $path_prefix);
      $changed = TRUE;
    }
    if ($changed) {
      $config->save();
      $this->routeBuilder->rebuild();
    }
    parent::submitForm($form, $form_state);
  }

}
