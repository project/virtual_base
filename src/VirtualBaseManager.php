<?php

namespace Drupal\virtual_base;

use Drupal\Component\Utility\Html;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Helper service for the Virtual Base Module.
 *
 *  @SuppressWarnings(PHPMD.CamelCaseParameterName)
 *  @SuppressWarnings(PHPMD.CamelCaseVariableName)
 *  @SuppressWarnings(PHPMD.LongClassName)
 *  @SuppressWarnings(PHPMD.LongVariable)
 */
class VirtualBaseManager implements VirtualBaseManagerInterface {

  public function __construct(
    protected ConfigFactoryInterface $configFactory,
    protected CacheBackendInterface $cacheBackend,
  ) {
  }

  /**
   * Get the config (no caching).
   *
   * @return \Drupal\Core\Config\ImmutableConfig
   *   Config.
   */
  protected function getConfigImpl() {
    return $this->configFactory->get('virtual_base.settings');
  }

  /**
   * {@inheritDoc}
   */
  public function getConfig() {
    $cid = 'virtual_base:settings';
    $staticCache = &drupal_static(__FUNCTION__ . $cid, NULL);
    if ($staticCache) {
      // If the static cache exists, then return it.
      return $staticCache;
    }
    // Get the cache out of the database and return the data component.
    $result = $this->cacheBackend->get($cid);
    if ($result && $result->data !== NULL) {
      return $result->data;
    }

    $data = $this->getConfigImpl();

    // Store cache.
    $tags = [
      'config:virtual_base.settings',
    ];
    $this->cacheBackend->set($cid, $data, CacheBackendInterface::CACHE_PERMANENT, $tags);
    return $data;
  }

  /**
   * {@inheritDoc}
   */
  public function getPathPrefix(bool $checkEnv = TRUE) : ?string {
    $config = $this->getConfig();
    $path_prefix = $config->get("enabled") ? $config->get("path_prefix") : NULL;
    if ($checkEnv) {
      $path_prefix_env = getenv('VIRTUAL_BASE');
      if (!$path_prefix_env || ($path_prefix_env != $path_prefix)) {
        return NULL;
      }
    }
    return $path_prefix ?: NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function resolveInboundPath($path): string {
    $prefix = $this->getPathPrefix(FALSE);
    if (!str_starts_with($path, $prefix)) {
      return $path;
    }

    return substr($path, strlen($prefix));
  }

  /**
   * {@inheritDoc}
   */
  public function resolveOutboundPath($path, CacheableMetadata $bubbleableMetadata = NULL): string {
    $prefix = $this->getPathPrefix();
    if ($bubbleableMetadata) {
      $bubbleableMetadata->addCacheContexts(['virtual_base']);
    }
    if ($prefix) {
      return $prefix . $path;
    }
    return $path;
  }

  /**
   * {@inheritDoc}
   */
  public function alterHtml(string $path_prefix, string $text) : ?string {
    $html_dom = Html::load($text);
    $somethingDone = FALSE;
    // $xpath = new \DOMXPath($html_dom);
    // foreach ($xpath->query('//' . $allowed_tag . '[@*]') as $element) {
    // $this->filterElementAttributes($element, $allowed_attributes);
    // }
    // Get all links inside html and transform the href.
    $this->changeAttributeTag($html_dom, $somethingDone, $path_prefix, 'a', 'href');
    $this->changeAttributeTag($html_dom, $somethingDone, $path_prefix, 'link', 'href');
    // Get all image inside html and transform the src.
    $this->changeAttributeTag($html_dom, $somethingDone, $path_prefix, 'img', 'src');
    $this->changeAttributeTag($html_dom, $somethingDone, $path_prefix, 'svg', 'src');
    // Get all script inside html and transform the src.
    $this->changeAttributeTag($html_dom, $somethingDone, $path_prefix, 'script', 'src');

    return $somethingDone ? Html::serialize($html_dom) : NULL;
  }

  /**
   * Rewrite attribute of an html tag with the virtual base prefix.
   *
   * @param \DOMDocument $html_dom
   *   DOM document.
   * @param bool $change_dom
   *   Change DOM?
   * @param string $path_prefix
   *   Path prefix.
   * @param string $tag_name
   *   Tag name.
   * @param string $attribute_name
   *   Attribute name.
   *
   * @return void
   *   Void.
   */
  private function changeAttributeTag(\DOMDocument $html_dom, bool &$change_dom, string $path_prefix, string $tag_name, string $attribute_name) : void {
    $tags = $html_dom->getElementsByTagName($tag_name);
    foreach ($tags as $tag) {
      if (!$tag->hasAttribute($attribute_name)) {
        continue;
      }
      $attribute = $tag->getAttribute($attribute_name);
      if (str_starts_with($attribute, '/') &&
        !str_starts_with($attribute, '//') &&
        !str_starts_with($attribute, $path_prefix)) {
        $tag->setAttribute($attribute_name, $path_prefix . $attribute);
        $change_dom = TRUE;
      }
    }
  }

}
