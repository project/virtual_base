<?php

namespace Drupal\virtual_base\PathProcessor;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\PathProcessor\OutboundPathProcessorInterface;
use Drupal\Core\PathProcessor\PathProcessorFront;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\path_alias\AliasManagerInterface;
use Drupal\virtual_base\VirtualBaseManagerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Processes the inbound path using path alias lookups.
 *
 *  @SuppressWarnings(PHPMD.CamelCaseParameterName)
 *  @SuppressWarnings(PHPMD.CamelCaseVariableName)
 *  @SuppressWarnings(PHPMD.LongClassName)
 *  @SuppressWarnings(PHPMD.LongVariable)
 */
class VirtualBasePathProcessor extends PathProcessorFront implements OutboundPathProcessorInterface {

  /**
   * An alias manager for looking up the system path.
   *
   * @var \Drupal\virtual_base\VirtualBaseManagerInterface
   */
  protected $helper;

  /**
   * Module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Alias manager.
   *
   * @var \Drupal\path_alias\AliasManagerInterface|null
   */
  protected $pathAliasManager;

  /**
   * Constructs a VirtualBasePathProcessor object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   Config.
   * @param \Drupal\virtual_base\VirtualBaseManagerInterface $helper
   *   Helper service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   Module handler.
   * @param \Drupal\path_alias\AliasManagerInterface|null $path_alias
   *   Path alias manager.
   */
  public function __construct(ConfigFactoryInterface $config, VirtualBaseManagerInterface $helper, ModuleHandlerInterface $module_handler, AliasManagerInterface $path_alias = NULL) {
    parent::__construct($config);
    $this->helper = $helper;
    $this->moduleHandler = $module_handler;
    $this->pathAliasManager = $path_alias;
  }

  /**
   * {@inheritdoc}
   */
  public function processInbound($path, Request $request) {
    // Get the real path requested.
    $path = $this->getPathAlias($path);
    $path2 = $this->helper->resolveInboundPath($path);
    if ($path2 === "/" || $path2 === "") {
      $path2 = parent::processInbound("/", $request);
    }
    return $path2;
  }

  /**
   * Get Path Alias.
   *
   * @param string $path
   *   Path.
   *
   * @return string
   *   Path Alias.
   */
  private function getPathAlias($path) {
    if (!$this->moduleHandler->moduleExists('path_alias')) {
      return $path;
    }
    // Path alias is enabled.
    if (!$this->pathAliasManager instanceof AliasManagerInterface) {
      return $path;
    }
    $path_alias = $this->pathAliasManager->getPathByAlias($path);
    if ($path_alias !== $path) {
      return $path_alias;
    }
    return $path;
  }

  /**
   * {@inheritdoc}
   */
  public function processOutbound($path, &$options = [], Request $request = NULL, BubbleableMetadata $bubbleable_metadata = NULL) {
    // Return the path with the virtual base prefix if found inside request.
    return $this->helper->resolveOutboundPath($path, $bubbleable_metadata);
  }

}
